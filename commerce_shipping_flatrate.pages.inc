<?php
/**
 *  commerce_shipping_flatrate.pages
 *
 *  @author blazey http://drupal.org/user/353861
 */

/**
 * Shipping options form.
 * Users with 'set flatrate shipping options' permission can set available
 * options here.
 */
function commerce_shipping_flatrate_options_form($form, &$form_state) {
  $form['shipping_options'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="shipping-options">',
    '#suffix' => '</div>',
  );

  if (isset($form_state['values'])) {
    $values = $form_state['values']['shipping_options'];
  } else {
    $values = _commerce_shipping_flatrate_get_setting('commerce_shipping_flatrate_options');
  }

  foreach ($values as $option) {
    $form['shipping_options'][] = _commerce_shipping_flatrate_option_fieldset(
            $option['name'], $option['cost']);
  }

  if (isset($form_state['clicked_button']) &&
          $form_state['clicked_button']['#value'] == t('Add another')) {
    $form['shipping_options'][] = _commerce_shipping_flatrate_option_fieldset();
    $form_state['options_count'] = count($values);
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['add_another'] = array(
    '#name' => 'add_another',
    '#type' => 'button',
    '#value' => t('Add another'),
    '#ajax' => array(
      'callback' => 'commerce_shipping_flatrate_options_form_ajax',
      'wrapper' => 'shipping-options',
      'method' => 'append',
      'effect' => 'slide',
    ),
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#validate' => array('commerce_shipping_flatrate_options_validate'),
    '#submit' => array('commerce_shipping_flatrate_options_submit'),
  );

  return $form;
}

/**
 * Returns single shipping option fieldset.
 */
function _commerce_shipping_flatrate_option_fieldset($name = '', $cost = '') {
  $form = array(
    '#type' => 'fieldset',
    '#title' => t(''),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $name,
    '#size' => 20,
    '#maxlength' => 128,
    '#required' => FALSE,
  );
  $form['cost'] = array(
    '#type' => 'textfield',
    '#title' => t('Cost'),
    '#default_value' => $cost,
    '#size' => 5,
    '#maxlength' => 10,
    '#required' => FALSE,
  );
  return $form;
}

/**
 * Add more button ajax callback.
 * Return newly added option fieldset to be appended to shipping options.
 */
function commerce_shipping_flatrate_options_form_ajax($form, $form_state) {
  return $form['shipping_options'][$form_state['options_count']];
}

/**
 * Options form validation.
 */
function commerce_shipping_flatrate_options_validate($form, &$form_state) {
  foreach ($form_state['values']['shipping_options'] as $key => $option) {
    $name = trim($option['name']);
    $cost = trim($option['cost']);
    // Check if both values are filled.
    if ((empty($name) && !empty($cost)) ||
        (!empty($name) && empty($cost))) {
      form_set_error("shipping_options][$key", t('Incorrect setting. Both
        values should be given. If you want to remove this shipping option clear
        the name and cost fields.'));

      // Check if cost is a number.
      if (!is_numeric($cost)) {
        form_set_error("shipping_options][$key][cost", t('Cost has to a number.'));
      }
    }
  }
}

/**
 * Options form submit handler.
 * Saves shipping options settings.
 */
function commerce_shipping_flatrate_options_submit($form, &$form_state) {
  $options = array();
  foreach ($form_state['values']['shipping_options'] as $option) {
    $name = trim($option['name']);
    $cost = trim($option['cost']);
    if (!empty($name) && !empty($cost)) {
      $options[] = array(
        'name' => $name,
        'cost' => $cost,
      );
    }
  }
  variable_set('commerce_shipping_flatrate_options', $options);
}