<?php
/**
 *  commerce_shipping_flatrate
 *  Flatrate shipping option for drupal commerce.
 *  @author blazey http://drupal.org/user/353861
 */

/**
 * Implements hook_permission().
 */
function commerce_shipping_flatrate_permission() {
  return array(
    'set flatrate shipping options' => array(
      'title' => t('Set shipping options'),
      'description' => t('Allows users to add, remove and change price of flatrate shipping options.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_menu().
 */
function commerce_shipping_flatrate_menu() {
  $items['admin/commerce/config/shipping-methods/shipping-flatrate'] = array(
    'title' => 'Flatrate shipping options',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_shipping_flatrate_options_form'),
    'access arguments' => array('set flatrate shipping options'),
    'file' => 'commerce_shipping_flatrate.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_commerce_shipping_method_info().
 */
function commerce_shipping_flatrate_commerce_shipping_method_info() {
  $shipping_methods = array();

  $shipping_methods['commerce_shipping_flatrate'] = array(
    'title' => t('Flatrate shipping method'),
    'description' => t('flatrate shipping method with multiple options'),
    'calculate_shipping' => 'commerce_shipping_flatrate_commerce_calculate_shipping',
  );

  return $shipping_methods;
}

/**
 * @param $stored_values
 *  An array of the values stored in the rule settings
 *
 * @return Form settings definition
 */
function commerce_shipping_flatrate_settings_form($stored_values) {
  $form = array();

  return $form;
}

/**
 * Shipping method callback: checkout form.
 */
function commerce_shipping_flatrate_submit_form($shipping_method, $pane_values, $checkout_pane, $order) {
  $form = array();

  // Merge in values from the order.
//  if (!empty($order->data['commerce_shipping_flatrate'])) {
//    $pane_values += $order->data['commerce_shipping_flatrate'];
//  }

  // Merge in default values.
//  $pane_values += array(
//    'type' => '',
//  );

  // Fill radios with available shipping options.
  $options = _commerce_shipping_flatrate_get_setting('commerce_shipping_flatrate_options');
  foreach ($options as $option) {
    $radios_options[$option['name']] = $option['name'] . ' - ' . $option['cost'] . ' ' . commerce_default_currency();
  }
  $form['shipping_option'] = array(
    '#type' => 'radios',
    '#title' => t('Select shipping option'),
    '#options' => $radios_options,
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Shipping method callback: calculate shipping.
 *   Calculate shipping price for an order.
 *
 * @param $settings.
 *    The settings saved for the rule triggering the shipping calculation.
 * @param $order.
 *    The fully loaded order
 * @param $currency_code.
 *    The suggested currency code to use for the shipping method price calculation.
 *
 * @return array of values, one for each line item created containing one of.
 *  - (array) containing any of these keys: 'amount', 'currency_code', 'quantity', 'label'
 *  - (int) the price of the shipping, the order currency will be used, formatted as integer.
 *
 * @see commerce_currency_amount_to_integer
 */
function commerce_shipping_flatrate_calculate_shipping($settings, $order, $currency_code) {
  // For this simple method, the price is always 42.
  $shipping_line_items = array();
  $options = _commerce_shipping_flatrate_get_setting('commerce_shipping_flatrate_options');
  foreach ($options as $option) {
    if ($option['name'] == $settings['checkout_settings']['shipping_option']) {
      $selected_shipping_option = $option;
    }
  }
  $amount = $selected_shipping_option['cost'];
  $shipping_line_items[] = array(
    'amount' => commerce_currency_decimal_to_amount($amount, $currency_code),
    'currency_code' => $currency_code,
    'label' => $selected_shipping_option['name'],
  );
  return $shipping_line_items;
}

/**
 * Gets module settings.
 *
 * @param $name
 *    Variable name.
 */
function _commerce_shipping_flatrate_get_setting($name) {
  switch ($name) {

    case 'commerce_shipping_flatrate_options':
      return variable_get('commerce_shipping_flatrate_options', array(array('name' => '', 'cost' => '')));

    default:
      watchdog('_commerce_shipping_flatrate_get_setting', "Tring to get value of
        undefined variable", NULL, WATCHDOG_WARNING);

  }
}